const sofas = [
  {
    color: "green",
    imageUrl: "assets/sofas/green_3.png",
    type: "3 Seater sofa",
    price: "2500",
  },
  {
    color: "green",
    imageUrl: "assets/sofas/green_1.png",
    type: "Single Chair",
    price: "1500",
  },
  {
    color: "green",
    imageUrl: "assets/sofas/green_2.png",
    type: "2 Seater sofa",
    price: "2500",
    highlighted: true,
  },
  {
    color: "green",
    imageUrl: "assets/sofas/green_3.png",
    type: "3 Seater sofa",
    price: "2500",
  },
  {
    color: "navy",
    imageUrl: "assets/sofas/navy_3.png",
    type: "3 Seater sofa",
    price: "2500",
  },
  {
    color: "navy",
    imageUrl: "assets/sofas/navy_1.png",
    type: "Single Chair",
    price: "1500",
  },
  {
    color: "navy",
    imageUrl: "assets/sofas/navy_2.png",
    type: "2 Seater sofa",
    price: "2500",
    highlighted: true,
  },
  {
    color: "navy",
    imageUrl: "assets/sofas/navy_3.png",
    type: "3 Seater sofa",
    price: "2500",
  },

  {
    color: "grey",
    imageUrl: "assets/sofas/grey_3.png",
    type: "3 Seater sofa",
    price: "2500",
  },
  {
    color: "grey",
    imageUrl: "assets/sofas/grey_1.png",
    type: "Single Chair",
    price: "1500",
  },
  {
    color: "grey",
    imageUrl: "assets/sofas/grey_2.png",
    type: "2 Seater sofa",
    price: "2500",
    highlighted: true,
  },
  {
    color: "grey",
    imageUrl: "assets/sofas/grey_3.png",
    type: "3 Seater sofa",
    price: "2500",
  },

  {
    color: "beige",
    imageUrl: "assets/sofas/beige_3.png",
    type: "3 Seater sofa",
    price: "2500",
  },
  {
    color: "beige",
    imageUrl: "assets/sofas/beige_1.png",
    type: "Single Chair",
    price: "1500",
  },
  {
    color: "beige",
    imageUrl: "assets/sofas/beige_2.png",
    type: "2 Seater sofa",
    price: "2500",
    highlighted: true,
  },
  {
    color: "beige",
    imageUrl: "assets/sofas/beige_3.png",
    type: "3 Seater sofa",
    price: "2500",
  },
];
const windowIsLessThan700 = window.matchMedia("(max-width: 700px)").matches;
const windowIsLessThan991 = window.matchMedia("(max-width: 991px)").matches;

const shuffle = (array) => array.sort(() => 0.5 - Math.random());

const updateGallery = (selectedColor) => {
  const htmlGalleryThumbs = document.querySelector("[data-gallery-thumbs]");
  const htmlGalleryHighlight = document.querySelector(
    "[data-gallery-highlight]"
  );
  const dataThumbs = sofas.filter(({ color }) => color === selectedColor);
  const dataHighlight = dataThumbs.filter(({ highlighted }) => highlighted);
  const thumbWidth = htmlGalleryThumbs.offsetWidth / 2.1;

  shuffle(dataThumbs);

  htmlGalleryThumbs.innerHTML = "";
  htmlGalleryHighlight.innerHTML = "";

  /* map thumbs */
  dataThumbs.map((data, i) => {
    const htmlImg = document.createElement("img");
    const htmlDiv = document.createElement("div");
    const htmlDivDiv = document.createElement("div");
    const htmlDivDivSpan1 = document.createElement("span");
    const htmlDivDivSpan2 = document.createElement("span");
    /* add data */
    htmlImg.src = data.imageUrl;
    htmlDivDivSpan1.innerHTML = data.type;
    htmlDivDivSpan2.innerHTML = `Desde ${data.price} &euro;`;
    /* add styles */
    htmlDiv.classList.add("block-grey");
    htmlDiv.classList.add("fade-in");
    setTimeout(() => htmlDiv.classList.add("scale-in"), 50 * i);
    htmlDivDiv.classList.add("description-tags");
    htmlDivDiv.classList.add("fade-in");
    htmlDiv.style.width = windowIsLessThan700 ? "100%" : `${thumbWidth}px`;
    htmlDiv.style.height = windowIsLessThan700 ? "auto" : `${thumbWidth}px`;
    /* append elements */
    htmlDivDiv.append(htmlDivDivSpan1, htmlDivDivSpan2);
    setTimeout(() => htmlDiv.append(htmlImg, htmlDivDiv), 50 * i);
    htmlGalleryThumbs.append(htmlDiv);
  });

  /* set highlight image */
  const htmlDiv = document.createElement("div");
  const htmlImg = document.createElement("img");
  const htmlDivDiv = document.createElement("div");
  const htmlDivDivSpan1 = document.createElement("span");
  const htmlDivDivSpan2 = document.createElement("span");
  /* add data */
  htmlImg.src = dataHighlight[0].imageUrl;
  htmlDivDivSpan1.innerHTML = dataHighlight[0].type;
  htmlDivDivSpan2.innerHTML = `Desde ${dataHighlight[0].price} &euro;`;
  /* add styles */
  htmlDivDiv.classList.add("description-tags");
  htmlDivDiv.classList.add("fade-in");
  htmlDiv.classList.add("block-grey");
  htmlDiv.classList.add("scale-in");
  htmlGalleryHighlight.classList.add("scale-in");
  /* append elements */
  htmlDivDiv.append(htmlDivDivSpan1, htmlDivDivSpan2);
  htmlDiv.append(htmlImg, htmlDivDiv);
  htmlGalleryHighlight.appendChild(htmlDiv);
};

const validateForm = () => {
  const val = document.forms["form-newsletter"]["email"].value;
  if (val == "") {
    alert("Please fill in your email");
    return false;
  }
};

const hideNav = () => {
  if (!windowIsLessThan991) return;
  document.querySelector("[data-nav-checkbox]").checked = false;
};

/* click outside hamburger nav will close the navigation menu */
document.addEventListener("click", function (event) {
  const nav = document.querySelector("[data-nav]");
  if (!nav.contains(event.target) && windowIsLessThan991) {
    hideNav();
  }
});

/*  run default color once on load */
updateGallery(document.querySelector('input[name="color"]:checked').value);

/* scroll events */
let scrollPosition = 0;
window.addEventListener("scroll", () => {
  /* *** fading in and out of header */
  const body = document.querySelector("[data-body]");
  const currentScroll = window.pageYOffset;

  if (currentScroll <= 0) {
    body.dataset.scrollstatus = "up";
    return;
  }

  if (currentScroll > scrollPosition && body.dataset.scrollstatus !== "down") {
    body.dataset.scrollstatus = "down";
  } else if (
    currentScroll < scrollPosition &&
    body.dataset.scrollstatus === "down"
  ) {
    body.dataset.scrollstatus = "up";
  }
  scrollPosition = currentScroll;

  /* *** Animate content when in viewport */
  const fadeInElements = document.querySelectorAll("[data-fadein]");
  // only do animations on desktop
  if (windowIsLessThan991) return;
  Array.from(fadeInElements).map((element) => {
    const position = element.getBoundingClientRect();

    // checking whether fully visible
    //if (position.top >= 0 && position.bottom <= window.innerHeight) {  }

    // checking whether out of visibility
    if (
      position.bottom >
        (window.innerHeight || document.documentElement.clientHeight) ||
      position.top <
        (window.innerHeight || document.documentElement.clientHeight)
    ) {
      element.classList.remove("fade-in");
    }
    // checking for partial visibility
    if (position.top < window.innerHeight && position.bottom >= 0) {
      element.classList.add("fade-in");
    }
  });
});
