# crisp

Crisp assignment

## Demo
[https://crisp-studioo.netlify.app/](https://crisp-studioo.netlify.app/)
## Design

[https://www.figma.com/file/ecKC0w9bxoxuDdeNjLvYfo/CRISP-TEST](https://www.figma.com/file/ecKC0w9bxoxuDdeNjLvYfo/CRISP-TEST)

## Description

You only need to produce this one page, internal pages are not needed.
Requirements are:

● it must work and look pixel perfect to design on following browsers: Edge,
Chrome, Safari, Firefox
● it must be responsive (you decide how it should look and work on tablets &
mobiles)
● only pure js can be used for functionality where needed (no jquery or any other
libraries)
● please do not use any css libraries
● newsletter functionality is not needed but field must be functional
● for frontend BEM methodology
● Please avoid using any css or js libraries, frameworks or plugins for this task
● site must include subtle animations on content blocks to make it look more alive
and engaging
● we will be looking for high attention to detail and creating the result that is as close
to flawless as possible.